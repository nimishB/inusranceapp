import React, { Component } from 'react';

class CoverList extends Component {
    constructor(props) {
        super(props)

        this.state = {
            coverlist: this.props.coverList || [],
            filteredList: this.props.coverList || [],
        };
    };

    handleChangeEvent = (e) => {
        console.log(this.state.coverlist);
        const {coverlist} = this.state;
        const selectedValue = e.target.value;
        const filteredList = coverlist.filter(function(cover){
            return cover.type === selectedValue;
        });
        this.setState({filteredList: filteredList});
    }

    render() {
        const {filteredList} = this.state;
        const {coverIndex}= this.props;
        return (
            <div>
                <div className="panel panel-primary">
                    <div className="panel-heading">
                        <div className="btn-group pull-right filter-cover-btn">
                            <label for="policycovers">Filter by
                                <select
                                    id="policycovers"
                                    name="policycovers"
                                    class="form-control"
                                    onChange={this.handleChangeEvent}
                                >
                                    <option value="all">All Requests</option>
                                    <option value="included">Included</option>
                                    <option value="excluded">Excluded</option>
                                    <option value="optional">Optional</option>
                                </select>
                            </label>
                        </div>
                        <h3 className="panel-title" data-toggle="collapse"
                            data-target={"#"+ coverIndex + "collapseOne"}>Covers</h3>
                    </div>
                    <div id={coverIndex + "collapseOne"} class="panel-collapse collapse">
                        <div class="panel-body">
                            <ul className="list-unstyled list-inline cover-listing">
                                {filteredList.map((coverName, index) => (
                                    <li key={index}>
                                        <span className="badge badge-pill badge-primary">{coverName.name.toUpperCase()}</span>
                                    </li>
                                ))
                                }
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CoverList;
