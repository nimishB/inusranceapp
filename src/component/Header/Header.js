import React, { Component } from 'react';
import './header.css';
import profilePic from "./../../assets/img/profile-img.png";
import companyLogo from "./../../assets/img/company-logo.png";


class Header extends Component {
  render() {
    return (
      <div> 
          <header>
        <div className="container">
            <div className="row">
                <nav className="navbar navbar-default main-header">
                    <div className="container-fluid">
                        {/* <!-- Brand and toggle get grouped for better mobile display --> */}
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span className="sr-only">Toggle navigation</span>
                                <i className="fa fa-bars" aria-hidden="true"></i>
                            </button>
                            <a className="navbar-brand" href="index.html" title="MahindraRise-logo"><img src={companyLogo} alt="Insurance Company Logo" /></a>
                        </div>

                        {/* <!-- Collect the nav links, forms, and other content for toggling --> */}
                        <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul className="nav navbar-nav navbar-right menu-links">
                                <li className="active"><a href="/" title="Home Page"><i className="icon icon-icn_home"></i><span className="visible-xs"> Home</span></a></li>
                                <li><a href="#." title="Dashboard"><i className="icon icon-icn_options" aria-hidden="true"></i><span className="visible-xs"> Dashboard</span></a></li>
                                <li><a href="#." title="Booking"><i className="icon icon-icn_suitecase"></i><span className="visible-xs"> Booking </span></a></li>
                                <li className="dropdown">
                                    <div className="profile-info">
                                        <a href="#." className="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            <div className="user-set">
                                                <span className="profile-info"><img src={profilePic} alt="userProfile" title="User-Avatar"/></span>
                                                <div className="username">
                                                    Suvimal Sreenivasan
                                                    <span>Executive Officer</span>
                                                </div>
                                            </div>
                                        </a>
                                        <ul className="dropdown-menu">
                                            <li><a href="#."> Profile</a></li>
                                            <li><a href="#."> Logout </a></li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        {/* <!-- /.navbar-collapse --> */}
                    </div>
                    {/* <!-- /.container-fluid --> */}
                </nav>
            </div>
        </div>     
    </header>
      </div>
    );
  }
}

export default Header;
