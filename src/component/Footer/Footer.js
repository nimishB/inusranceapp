import React, { Component } from 'react';
import './footer.css';

class Footer extends Component {
  render() {
    return (
    <div>
    <section className="bottom-section padding-top">
        <div className="container">
            <div className="row">
                <div className="col-md-8 col-xs-12  footer-widget get-in-touch no-padding">
                    <div className="col-xs-12 col-sm-6 no-padding">
                        <h4> Get in Touch </h4>
                        <h5 className="venue">
                            <address>
                            Insurance Company &amp; Ltd.,<br/>
                            Automotive Division, AFS, Company Tower,<br/>
                            3rd Floor, Akurli Road,<br/>
                            Kandivali (East), Mumbai-400101
                        </address>
                        </h5>

                        <h6 className="mail-text"><a href="#." className="mail-link" title="customercare@company.com">customercare@company.com </a> </h6>

                    </div>
                    <div className="col-xs-12 col-sm-6 no-padding">
                        <h5 className="shout-at">
                            <address>
                            022-28849588 | 022-28468525<br/>
                            Fax: 022-28468523<br/>
                            Office Hrs.: 09.00-18.00
                        </address>
                        </h5>
                        <a href="https://twitter.com/" title="We are Social" className="twitter-icon " target="_blank">
                        </a>
                        <h6 className="social-link" title="We are Social"> We are Social.</h6>
                    </div>
                </div>
                <div className="col-md-offset-1 col-md-3 col-xs-12 footer-widget quick-links no-padding">
                    <h4> Quick Links </h4>
                    <div className="col-sm-4 col-md-6 col-xs-12 no-padding">
                        <ul className="list-unstyled">
                            <li><a href="#javscript:void(0);" title="Home"> Home </a></li>
                            <li><a href="#javscript:void(0);" title="About Us"> About Us </a></li>
                            <li><a href="#javscript:void(0);" title="Our Investors"> Our Investors </a></li>
                            <li><a href="#javscript:void(0);" title="Our Blog"> Our Blog </a></li>
                            <li><a href="#javscript:void(0);" title="Useful Links"> Useful Links </a></li>
                        </ul>
                    </div>
                    <div className="col-sm-offset-2 col-sm-4 col-md-4 col-xs-12 no-padding">
                        <ul className="list-unstyled">
                            <li><a href="#javscript:void(0);" title="FAQ's"> FAQ's </a></li>
                            <li><a href="#javscript:void(0);" title="Terms of Use"> Terms of Use </a></li>
                            <li><a href="#javscript:void(0);" title="Privacy Policy"> Privacy Policy </a></li>
                            <li><a href="#javscript:void(0);" title="Disclaimer"> Disclaimer </a></li>
                            <li><a href="#javscript:void(0);" title="Site Map"> Site Map </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <footer className="footer-wrap">
        <div className="container">
            <div className="row">
                <p className="footer-text"> Copyright &copy; 2016 <a href="javascript:void(0);" title="Insurance Company Rise"> Insurance Company </a> All rights reserved. </p>
            </div>
        </div>
    </footer>
    </div>
    );
  }
}

export default Footer;
