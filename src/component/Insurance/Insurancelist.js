import React, { Component } from 'react';
import './insurance.css';
import placeholderImg from "../../assets/img/placeholder-image.png"
import Inusrancelist from '../../shared/insurance';
import CoverList from '../CoverList/coverList';


class Insurancelist extends Component {

    constructor(props) {
        super(props)

        this.state = {
            value: '',
            covers: []
        };
        this.selectedValue = ''
    };
    render() {
        return (
            <div>
                <section className="policy-list-wrapper">
                    <div className="container">
                        <div className="row">
                            <div className=" col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
                                <h6> 26 Insurance Policies available</h6>
                                <ul className="list-unstyled list-inline item-list">
                                    {Inusrancelist.map((insurance,index) => (
                                        <li key={insurance.id}>
                                            <div className="item-thread">
                                                <div className="col-xs-12 col-sm-3 col-md-3 col-lg-3 no-padding">
                                                    <div className="policy-images-wrapper">
                                                        <img src={placeholderImg} alt="media" className="img-responive policy-img" />
                                                        <div className="like-box">
                                                            <i className="fa fa-heart" aria-hidden="true"></i>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col-xs-12 col-sm-9 col-md-9 col-lg-9 no-padding description-wrapper">
                                                    <div className="col-xs-12 col-sm-12 col-md-12">
                                                        <div className="img-policybox col-xs-2">
                                                            <i className="icon icon-3x icon-icn_accomodation home-icon"></i>
                                                        </div>
                                                        <div className="insu-policy-description col-xs-10">
                                                            <h2> {insurance.provider['en-ae'].product_name}</h2>
                                                            <p className="short-info"> {insurance.provider['en-ae'].name}</p>
                                                        </div>
                                                    </div>
                                                    <div className="col-xs-12 col-sm-12 col-md-12 facilities">
                                                        <ul className="list-unstyled list-inline facilities-listing">
                                                            <li>

                                                                <label> Insurance Type</label>
                                                                <span> {insurance.insurance_type} </span>

                                                            </li>
                                                            <li>

                                                                <label>Provider</label>
                                                                <span> {insurance.provider['en-ae'].name} </span>

                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div className="box-title common-price-box corporate-price">
                                                        <a href="#." className="insuranceTypeModal" title="Corporate Price" data-toggle="modal" data-target="#priortyModal">
                                                            <div className="price-tag greenbg">
                                                                <p className="rotate"> Add to Compare </p>
                                                            </div>

                                                            <h1 className="text-green">{insurance.premium}<span className="aed text-green">{insurance.currency}</span></h1>
                                                            <span>Per month</span>
                                                            <p className="alert-text">*Please choose to compare</p>
                                                        </a>
                                                    </div>
                                                    <div className="col-xs-12 col-sm-12 col-md-12 listed-cover">
                                                        <CoverList coverList={insurance.covers} coverIndex={index}/>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    ))
                                    }
                                </ul>
                            </div>
                            <div className="col-md-12 col-sm-12 col-xs-12 paginations-bar">
                                <nav aria-label="Page navigation">
                                    <ul className="pagination">
                                        <li className="active"><a href="#.">1</a></li>
                                        <li><a href="#.">2</a></li>
                                        <li><a href="#.">3</a></li>
                                        <li><a href="#.">4</a></li>
                                        <li><a href="#.">5</a></li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </section>
                <section className="priority-selection">
                    <div className="container">
                        <div className="row">
                            <p>Your Priority List </p>
                            <div className="col-xs-12 col-sm-3 col-md-3 priorty-widget-box">
                                <div className="media" id="priorty1">
                                    <a className="pull-left" href="#.">
                                        <img className="media-object" src="images/04.png" alt="Media Object" />
                                    </a>
                                    <div className="media-body priorty-body">
                                        <button type="button" className="close closePriorty"><span aria-hidden="true">&times;</span></button>
                                        <ul className="list-unstyled list-inline insurance-desc">
                                            <li><span>HDFC Life </span></li>
                                            <li> Provider Name </li>
                                        </ul>
                                        <ul className="list-unstyled list-inline inusrance-pricing">
                                            <li><i className="fa fa-rupee"></i> 2233.00</li>
                                            <li><span className="label priorty-corporate-label ">Insurance Type</span> </li>
                                        </ul>
                                    </div>
                                </div>
                                <span className="badge priorty-badge"> 1 </span>
                            </div>
                            <div className="col-xs-12 col-sm-3 col-md-3 priorty-widget-box">
                                <div className="media" id="priorty2">
                                    <a className="pull-left" href="#.">
                                        <img className="media-object" src="images/03.png" alt="Media Object" />
                                    </a>
                                    <div className="media-body priorty-body">
                                        <button type="button" className="close closePriorty"><span aria-hidden="true">&times;</span></button>
                                        <ul className="list-unstyled list-inline insurance-desc">
                                            <li><span>SBI Life </span></li>
                                            <li> Provider Name </li>
                                        </ul>
                                        <ul className="list-unstyled list-inline inusrance-pricing">
                                            <li><i className="fa fa-rupee"></i> 2233.00</li>
                                            <li><span className="label priorty-corporate-label">Insurance Type</span> </li>
                                        </ul>
                                    </div>
                                </div>
                                <span className="badge priorty-badge"> 2 </span>
                            </div>
                            <div className="col-xs-12 col-sm-3 col-md-3 priorty-widget-box">
                                <div className="media" id="priorty3">
                                    <a className="pull-left" href="#.">
                                        <img className="media-object" src="images/05.png" alt="Media Object" />
                                    </a>
                                    <div className="media-body priorty-body">
                                        <button type="button" className="close closePriorty"><span aria-hidden="true">&times;</span></button>
                                        <ul className="list-unstyled list-inline inusrance-desc">
                                            <li><span>HDFC Life </span></li>
                                            <li> Provider Name </li>
                                        </ul>
                                        <ul className="list-unstyled list-inline inusrance-pricing">
                                            <li><i className="fa fa-rupee"></i> 2233.00</li>
                                            <li><span className="label priorty-corporate-label ">Insurance Type</span> </li>
                                        </ul>
                                    </div>
                                </div>
                                <span className="badge priorty-badge"> 3 </span>
                            </div>
                            <div className="col-xs-12 col-sm-2 col-md-2 submit-btn">
                                <button type="button" id="sendRequest" title="Send Request" className="btn btn-default request-btn"> Compare </button>
                            </div>
                        </div>
                    </div>
                </section>
                {/*Modal pop Up for Priority Alert */}
                <div className="modal" id="priortyModal" tabIndex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content priorty-modal-content">
                            <div className="modal-body text-center">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h3 className="greet-text">Great Start!</h3>
                                <p>You have chosen your first priority.</p>
                                <p>You can select one more Insurance to compare.</p>
                                <div className="submit-btn">
                                    <button data-dismiss="modal" aria-label="Close" type="button" id="sendRequest" title="Send Request" className="btn btn-default request-btn">Ok</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Insurancelist;
