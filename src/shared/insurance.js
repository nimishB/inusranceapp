const Inusrancelist = [
    {
       "tpl":1100,
       "insurance_type":"comprehensive",
       "premium":4516,
       "provider":{
          "en-ae":{
             "url_slug":"watania",
             "terms_conditions_url":"https://bnkn.us/insurance-terms/WataniaTerms.pdf",
             "image_url":"https://static4.bnkn.us.com/ci/providers/watania.png",
             "name":"Watania",
             "description":"National Takaful Company (Watania) is a young and dynamic organization that came into being in 2011. Watania\u2019s General Takaful department is responsible for all classes of insurance other than Motor and Medical. Resourced with highly experienced staff, this department is dedicated to serve mainly the commercial insurance needs of the market. The department has developed a wide a range of products to cater to the requirement of business of all sizes and types in all the major sectors of the market. All the products are Shariah compliant.",
             "time_to_policy":1,
             "product_name":"Watania Premium (Garage)"
          }
       },
       "excess":1000,
       "currency":"AED",
       "id":"wataniapremium",
       "price_breakdown":{
          "modifiers":[
             {
                "terms":{
                   "starts_at":"2017-01-01T00:00:00.000+0000",
                   "product":"ALL",
                   "code":"",
                   "name":"Ramadan extended",
                   "ends_at":"2017-12-12T00:00:00.000+0000",
                   "type":"discountPercentage",
                   "value":"0.06"
                },
                "category":"voucher"
             }
          ],
          "base":4805
       },
       "covers":[
          {
             "name":"personal Accident For Driver",
             "options":[
                {
                   "cost":120
                }
             ],
             "type":"optional",
             "selected":false
          },
          {
             "name":"dent Repair",
             "type":"excluded"
          },
          {
             "name":"replacement Locks Covered",
             "type":"excluded"
          },
          {
             "name":"thirdParty Property Damage",
             "options":[
                {
                   "text":"Up to AED 2M"
                }
             ],
             "type":"included"
          },
          {
             "name":"omanExtension",
             "options":[
                {
                   "cost":1000
                }
             ],
             "type":"optional",
             "selected":false
          },
          {
             "name":"loss Or Damage To VehicleCovered",
             "type":"included"
          },
          {
             "name":"valet Parking TheftCover",
             "type":"excluded"
          },
          {
             "name":"third Party BodilyInjury Covered",
             "options":[
                {
                   "text":"Unlimited"
                }
             ],
             "type":"included"
          },
          {
             "name":"agency Repair",
             "type":"excluded"
          },
          {
             "name":"loss Of PersonalEffects",
             "type":"excluded"
          },
          {
             "name":"emergency Medical Expenses",
             "options":[
                {
                   "text":"Up to AED 2,000"
                }
             ],
             "type":"included"
          },
          {
             "name":"personal Accident For Passenger",
             "options":[
                {
                   "cost":150
                },
                {
                   "text":"Family and Friends included"
                }
             ],
             "type":"excluded",
             "selected":false
          },
          {
             "name":"ambulance Fees",
             "type":"included"
          },
          {
             "name":"natural Calamity Covered",
             "type":"excluded"
          },
          {
             "name":"auto Gap Insurance",
             "type":"excluded"
          },
          {
             "name":"riot Or Strikes Covered",
             "type":"excluded"
          },
          {
             "name":"gcc Covered",
             "type":"excluded"
          },
          {
             "name":"involuntary Employment Loss",
             "type":"excluded"
          },
          {
             "name":"car Rental",
             "options":[
                {
                   "cost":200
                }
             ],
             "type":"optional",
             "selected":false
          },
          {
             "name":"windScreen Damage",
             "options":[
                {
                   "text":"Up to AED 3,000"
                }
             ],
             "type":"included"
          },
          {
             "name":"noExcess For WindscreenDamage",
             "type":"included"
          },
          {
             "name":"roadside Assistance",
             "type":"included"
          },
          {
             "name":"fire And Theft Covered",
             "type":"included"
          },
          {
             "name":"protected No ClaimsDiscount",
             "type":"excluded"
          },
          {
             "name":"off road Covered",
             "type":"excluded"
          },
          {
             "name":"free Car RegistrationService",
             "type":"included"
          }
       ],
       "own_damage":3705
    },
    {
       "tpl":1100,
       "insurance_type":"comprehensive",
       "premium":5943,
       "provider":{
          "en-ae":{
             "url_slug":"watania",
             "terms_conditions_url":"https://static4.bnkn.us/insurance-terms/WataniaTerms.pdf",
             "image_url":"https://static4.bnkn.us/ci/providers/watania.png",
             "name":"Watania",
             "description":"National Takaful Company (Watania) is a young and dynamic organization that came into being in 2011. Watania\u2019s General Takaful department is responsible for all classes of insurance other than Motor and Medical. Resourced with highly experienced staff, this department is dedicated to serve mainly the commercial insurance needs of the market. The department has developed a wide a range of products to cater to the requirement of business of all sizes and types in all the major sectors of the market. All the products are Shariah compliant.",
             "time_to_policy":1,
             "product_name":"Watania Agency"
          }
       },
       "excess":1000,
       "currency":"AED",
       "id":"wataniaagency",
       "price_breakdown":{
          "modifiers":[
             {
                "terms":{
                   "starts_at":"2017-01-01T00:00:00.000+0000",
                   "product":"ALL",
                   "code":"",
                   "name":"Ramadan extended",
                   "ends_at":"2017-12-12T00:00:00.000+0000",
                   "type":"discountPercentage",
                   "value":"0.06"
                },
                "category":"voucher"
             }
          ],
          "base":6322
       },
       "covers":[
          {
             "name":"personal Accident ForDriver",
             "options":[
                {
                   "cost":120
                }
             ],
             "type":"optional",
             "selected":false
          },
          {
             "name":"dent Repair",
             "type":"excluded"
          },
          {
             "name":"replacement Locks Covered",
             "type":"excluded"
          },
          {
             "name":"thirdParty Property Damage",
             "options":[
                {
                   "text":"Up to AED 2M"
                }
             ],
             "type":"included"
          },
          {
             "name":"oman Extension",
             "options":[
                {
                   "cost":1000
                }
             ],
             "type":"optional",
             "selected":false
          },
          {
             "name":"lossOrDamage To Vehicle Covered",
             "type":"included"
          },
          {
             "name":"valetParking TheftCover",
             "type":"excluded"
          },
          {
             "name":"thirdParty BodilyInjury Covered",
             "options":[
                {
                   "text":"Unlimited"
                }
             ],
             "type":"included"
          },
          {
             "name":"agencyRepair",
             "type":"included"
          },
          {
             "name":"lossOf PersonalEffects",
             "type":"excluded"
          },
          {
             "name":"emergency Medical Expenses",
             "options":[
                {
                   "text":"Up to AED 2,000"
                }
             ],
             "type":"included"
          },
          {
             "name":"personal Accident For Passenger",
             "options":[
                {
                   "cost":150
                },
                {
                   "text":"Family and Friends included"
                }
             ],
             "type":"excluded",
             "selected":false
          },
          {
             "name":"ambulance Fees",
             "type":"included"
          },
          {
             "name":"natural Calamity Covered",
             "type":"excluded"
          },
          {
             "name":"autoGap Insurance",
             "type":"included"
          },
          {
             "name":"riot Or Strikes Covered",
             "type":"excluded"
          },
          {
             "name":"gcc Covered",
             "type":"excluded"
          },
          {
             "name":"involuntary EmploymentLoss",
             "type":"excluded"
          },
          {
             "name":"car Rental",
             "options":[
                {
                   "cost":200
                }
             ],
             "type":"optional",
             "selected":false
          },
          {
             "name":"windScreen Damage",
             "options":[
                {
                   "text":"Up to AED 3,000"
                }
             ],
             "type":"included"
          },
          {
             "name":"noExcess For Windscreen Damage",
             "type":"included"
          },
          {
             "name":"road side Assistance",
             "type":"included"
          },
          {
             "name":"fire And TheftCovered",
             "type":"included"
          },
          {
             "name":"protected No ClaimsDiscount",
             "type":"excluded"
          },
          {
             "name":"offroad Covered",
             "type":"excluded"
          },
          {
             "name":"free Car RegistrationService",
             "type":"included"
          }
       ],
       "own_damage":5222
    },
    {
       "tpl":1100,
       "insurance_type":"comprehensive",
       "premium":3506,
       "provider":{
          "en-ae":{
             "url_slug":"watania",
             "terms_conditions_url":"https://static4.bnkn.us/insurance-terms/WataniaTerms.pdf",
             "image_url":"https://static4.bnkn.us/ci/providers/watania.png",
             "name":"Watania",
             "description":"National Takaful Company (Watania) is a young and dynamic organization that came into being in 2011. Watania\u2019s General Takaful department is responsible for all classes of insurance other than Motor and Medical. Resourced with highly experienced staff, this department is dedicated to serve mainly the commercial insurance needs of the market. The department has developed a wide a range of products to cater to the requirement of business of all sizes and types in all the major sectors of the market. All the products are Shariah compliant.",
             "time_to_policy":1,
             "product_name":"Watania Garage"
          }
       },
       "excess":1000,
       "currency":"AED",
       "id":"wataniagarage",
       "price_breakdown":{
          "modifiers":[
             {
                "terms":{
                   "starts_at":"2017-01-01T00:00:00.000+0000",
                   "product":"ALL",
                   "code":"",
                   "name":"Ramadan extended",
                   "ends_at":"2017-12-12T00:00:00.000+0000",
                   "type":"discountPercentage",
                   "value":"0.06"
                },
                "category":"voucher"
             }
          ],
          "base":3730
       },
       "covers":[
          {
             "name":"personal Accident ForDriver",
             "options":[
                {
                   "cost":120
                }
             ],
             "type":"optional",
             "selected":false
          },
          {
             "name":"dent Repair",
             "type":"excluded"
          },
          {
             "name":"replacement LocksCovered",
             "type":"excluded"
          },
          {
             "name":"thirdParty PropertyDamage",
             "options":[
                {
                   "text":"Up to AED 2M"
                }
             ],
             "type":"included"
          },
          {
             "name":"oman Extension",
             "options":[
                {
                   "cost":1000
                }
             ],
             "type":"optional",
             "selected":false
          },
          {
             "name":"lossOrDamage To VehicleCovered",
             "type":"included"
          },
          {
             "name":"valetParking Theft Cover",
             "type":"excluded"
          },
          {
             "name":"thirdParty BodilyInjury Covered",
             "options":[
                {
                   "text":"Unlimited"
                }
             ],
             "type":"included"
          },
          {
             "name":"agency Repair",
             "type":"excluded"
          },
          {
             "name":"loss Of Personal Effects",
             "type":"excluded"
          },
          {
             "name":"emergency Medical Expenses",
             "options":[
                {
                   "text":"Up to AED 2,000"
                }
             ],
             "type":"included"
          },
          {
             "name":"personal Accident For Passenger",
             "options":[
                {
                   "cost":150
                },
                {
                   "text":"Family and Friends included"
                }
             ],
             "type":"excluded",
             "selected":false
          },
          {
             "name":"ambulance Fees",
             "type":"included"
          },
          {
             "name":"natural Calamity Covered",
             "type":"excluded"
          },
          {
             "name":"autoGap Insurance",
             "type":"excluded"
          },
          {
             "name":"riotOrStrikes Covered",
             "type":"excluded"
          },
          {
             "name":"gcc Covered",
             "type":"excluded"
          },
          {
             "name":"involuntary EmploymentLoss",
             "type":"excluded"
          },
          {
             "name":"car Rental",
             "options":[
                {
                   "cost":200
                }
             ],
             "type":"optional",
             "selected":false
          },
          {
             "name":"windScreen Damage",
             "options":[
                {
                   "text":"Up to AED 3,000"
                }
             ],
             "type":"included"
          },
          {
             "name":"noExcess For Windscreen Damage",
             "type":"included"
          },
          {
             "name":"roadside Assistance",
             "type":"included"
          },
          {
             "name":"fireAndTheft Covered",
             "type":"included"
          },
          {
             "name":"protected No ClaimsDiscount",
             "type":"excluded"
          },
          {
             "name":"offroad Covered",
             "type":"excluded"
          },
          {
             "name":"free Car RegistrationService",
             "type":"included"
          }
       ],
       "own_damage":2630
    },
    {
       "insurance_type":"comprehensive",
       "premium":3566,
       "provider":{
          "en-ae":{
             "url_slug":"union",
             "terms_conditions_url":"https://static4.bnkn.us/insurance-terms/UnionTerms.pdf",
             "image_url":"https://static4.bnkn.us/ci/providers/union-logo_19072017.png",
             "name":"Union",
             "description":"Established in 1998 and listed on the Abu Dhabi Securities Exchange, Union Insurance Company provides a wide range of individual and commercial insurance products to clients in the UAE and Middle East region. This includes both standard and customised policies that help individuals, small, medium and large enterprises as well as government entities obtain reliable, cost-effective insurance cover.",
             "time_to_policy":24,
             "product_name":"Union Silver"
          }
       },
       "excess":1000,
       "currency":"AED",
       "id":"unionsilver",
       "price_breakdown":{
          "modifiers":[
             {
                "terms":{
                   "starts_at":"2017-01-01T00:00:00.000+0000",
                   "product":"ALL",
                   "code":"",
                   "name":"Ramadan extended",
                   "ends_at":"2017-12-12T00:00:00.000+0000",
                   "type":"discountPercentage",
                   "value":"0.06"
                },
                "category":"voucher"
             }
          ],
          "base":3793
       },
       "covers":[
          {
             "name":"personal Accident For Driver",
             "options":[
                {
                   "cost":120
                }
             ],
             "type":"optional",
             "selected":false
          },
          {
             "name":"dent Repair",
             "type":"excluded"
          },
          {
             "name":"replacement LocksCovered",
             "type":"excluded"
          },
          {
             "name":"agency Repair",
             "options":[
                {
                   "cost":506
                }
             ],
             "type":"optional",
             "selected":false
          },
          {
             "name":"thirdParty PropertyDamage",
             "options":[
                {
                   "text":"Up to AED 2M"
                }
             ],
             "type":"included"
          },
          {
             "name":"lossOrDamage To VehicleCovered",
             "type":"included"
          },
          {
             "name":"valetParking TheftCover",
             "type":"excluded"
          },
          {
             "name":"thirdParty BodilyInjury Covered",
             "options":[
                {
                   "text":"Unlimited"
                }
             ],
             "type":"included"
          },
          {
             "name":"loss Of PersonalEffects",
             "type":"excluded"
          },
          {
             "name":"oman Extension",
             "type":"excluded"
          },
          {
             "name":"involuntary EmploymentLoss",
             "options":[
                {
                   "text":"Up to AED 5,000"
                }
             ],
             "type":"included"
          },
          {
             "name":"ambulance Fees",
             "type":"included"
          },
          {
             "name":"natural CalamityCovered",
             "type":"included"
          },
          {
             "name":"autoGap Insurance",
             "type":"included"
          },
          {
             "name":"riotOrStrikes Covered",
             "type":"included"
          },
          {
             "name":"gcc Covered",
             "type":"included"
          },
          {
             "name":"car Rental",
             "options":[
                {
                   "cost":175
                }
             ],
             "type":"optional",
             "selected":false
          },
          {
             "name":"windScreen Damage",
             "type":"excluded"
          },
          {
             "name":"personal Accident For Passenger",
             "options":[
                {
                   "text":"Family Only"
                }
             ],
             "type":"included"
          },
          {
             "name":"noExcess For Windscreen Damage",
             "type":"excluded"
          },
          {
             "name":"roadside Assistance",
             "type":"included"
          },
          {
             "name":"fireAndTheft Covered",
             "type":"included"
          },
          {
             "name":"protected NoClaims Discount",
             "type":"included"
          },
          {
             "name":"offroad Covered",
             "type":"excluded"
          },
          {
             "name":"emergency MedicalExpenses",
             "type":"excluded"
          },
          {
             "name":"freeCar RegistrationService",
             "type":"excluded"
          }
       ]
    },
    {
       "insurance_type":"comprehensive",
       "premium":3744,
       "provider":{
          "en-ae":{
             "url_slug":"union",
             "terms_conditions_url":"https://static4.bnkn.us/insurance-terms/UnionTerms.pdf",
             "image_url":"https://static4.bnkn.us/ci/providers/union-logo_19072017.png",
             "name":"Union",
             "description":"Established in 1998 and listed on the Abu Dhabi Securities Exchange, Union Insurance Company provides a wide range of individual and commercial insurance products to clients in the UAE and Middle East region. This includes both standard and customised policies that help individuals, small, medium and large enterprises as well as government entities obtain reliable, cost-effective insurance cover.",
             "time_to_policy":24,
             "product_name":"Union Gold"
          }
       },
       "excess":1000,
       "currency":"AED",
       "id":"uniongold",
       "price_breakdown":{
          "modifiers":[
             {
                "terms":{
                   "starts_at":"2017-01-01T00:00:00.000+0000",
                   "product":"ALL",
                   "code":"",
                   "name":"Ramadan extended",
                   "ends_at":"2017-12-12T00:00:00.000+0000",
                   "type":"discountPercentage",
                   "value":"0.06"
                },
                "category":"voucher"
             }
          ],
          "base":3983
       },
       "covers":[
          {
             "name":"personal Accident For Driver",
             "options":[
                {
                   "cost":120
                }
             ],
             "type":"optional",
             "selected":false
          },
          {
             "name":"gcc Covered",
             "options":[
                {
                   "text":"Oman and Qatar Only"
                }
             ],
             "type":"included"
          },
          {
             "name":"dent Repair",
             "type":"included"
          },
          {
             "name":"replacement Locks Covered",
             "options":[
                {
                   "text":"Up to AED 1,000"
                }
             ],
             "type":"included"
          },
          {
             "name":"agency Repair",
             "options":[
                {
                   "cost":443
                }
             ],
             "type":"optional",
             "selected":false
          },
          {
             "name":"thirdParty PropertyDamage",
             "options":[
                {
                   "text":"Up to AED 5M"
                }
             ],
             "type":"included"
          },
          {
             "name":"lossOrDamage To VehicleCovered",
             "type":"included"
          },
          {
             "name":"valetParking TheftCover",
             "type":"included"
          },
          {
             "name":"thirdParty BodilyInjury Covered",
             "options":[
                {
                   "text":"Unlimited"
                }
             ],
             "type":"included"
          },
          {
             "name":"lossOf Personal Effects",
             "options":[
                {
                   "text":"Up to AED 5,000"
                }
             ],
             "type":"included"
          },
          {
             "name":"emergency Medical Expenses",
             "options":[
                {
                   "text":"Up to AED 6,000"
                }
             ],
             "type":"included"
          },
          {
             "name":"omanExtension",
             "type":"included"
          },
          {
             "name":"involuntary Employment Loss",
             "options":[
                {
                   "text":"Up to AED 5,000"
                }
             ],
             "type":"included"
          },
          {
             "name":"ambulance Fees",
             "type":"included"
          },
          {
             "name":"naturalCalamity Covered",
             "type":"included"
          },
          {
             "name":"autoGap Insurance",
             "type":"included"
          },
          {
             "name":"riotOrStrikes Covered",
             "type":"included"
          },
          {
             "name":"car Rental",
             "options":[
                {
                   "cost":175
                }
             ],
             "type":"optional",
             "selected":false
          },
          {
             "name":"windScreen Damage",
             "type":"included"
          },
          {
             "name":"personal Accident For Passenger",
             "options":[
                {
                   "text":"Family Only"
                }
             ],
             "type":"included"
          },
          {
             "name":"noExcess For Windscreen Damage",
             "type":"included"
          },
          {
             "name":"roadside Assistance",
             "type":"included"
          },
          {
             "name":"fireAndTheft Covered",
             "type":"included"
          },
          {
             "name":"protected No Claims Discount",
             "type":"included"
          },
          {
             "name":"offroad Covered",
             "type":"excluded"
          },
          {
             "name":"freeCar Registration Service",
             "type":"included"
          }
       ]
    }
 ];

 export default Inusrancelist;