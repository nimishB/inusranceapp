import "./App.css";
import Header from "./component/Header/Header";
import Footer from "./component/Footer/Footer";
import Insurancelist from "./component/Insurance/Insurancelist";
// import Comparepolicy from "./component/Insurance/comparepolicy";


function App() {
  return (
    <>
      <Header />
      <Insurancelist/>
      {/* <Comparepolicy /> */}
      <Footer />
    </>
  );
}

export default App;
